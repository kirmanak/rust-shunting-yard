# Introduction

This is yet another Shunting-Yard algorithm implementation. 
Besides, it has an entry point and can evaluate the algorithm result, so one
can run it natively and calculate an expression.