use operator::Operator;
use std::ops::{Add, Div, Mul, Rem, Sub};
use std::str::FromStr;
use token::Token;

mod operator;
mod token;

pub fn evaluate_expression<T>(expression: &str) -> Result<T, ()>
    where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> + FromStr {
    match to_postfix(expression) {
        Ok(postfix) => evaluate_postfix(postfix),
        Err(err) => Err(err)
    }
}

pub fn evaluate_postfix<T>(mut tokens: Vec<Token<T>>) -> Result<T, ()>
    where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> + FromStr {
    let mut numbers: Vec<T> = Vec::with_capacity(tokens.len());
    for token in tokens.drain(..) {
        match token {
            Token::Number(num) => {
                numbers.push(num);
            }
            Token::Operator(operator) => {
                if operator != Operator::LeftBracket && operator != Operator::RightBracket {
                    if let Some(second) = numbers.pop() {
                        if let Some(first) = numbers.pop() {
                            numbers.push(operator.apply(first, second))
                        } else {
                            return Err(());
                        }
                    } else {
                        return Err(());
                    }
                }
            }
        }
    }

    if let Some(result) = numbers.pop() {
        if numbers.is_empty() {
            return Ok(result);
        }
    }
    Err(())
}

pub fn to_postfix<T>(expression: &str) -> Result<Vec<Token<T>>, ()>
    where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> + FromStr {
    let mut output: Vec<Token<T>> = Vec::with_capacity(expression.len());
    let mut operator_stack: Vec<Operator> = Vec::with_capacity(expression.len());

    for part in expression.split_whitespace() {
        if let Ok(token) = part.parse() {
            if let Token::Operator(operator) = token {
                match operator {
                    Operator::LeftBracket => {
                        operator_stack.push(operator);
                    }
                    Operator::RightBracket => {
                        loop {
                            if let Some(stack_head) = operator_stack.pop() {
                                if stack_head == Operator::LeftBracket {
                                    break;
                                }
                                output.push(Token::Operator(stack_head));
                            } else {
                                break;
                            }
                        }
                    }
                    _ => {
                        loop {
                            if let Some(stack_head) = operator_stack.pop() {
                                if stack_head < operator || stack_head == Operator::LeftBracket {
                                    operator_stack.push(stack_head);
                                    break;
                                }
                                output.push(Token::Operator(stack_head));
                            } else {
                                break;
                            }
                        }
                        operator_stack.push(operator);
                    }
                }
            } else {
                output.push(token);
            }
        } else {
            return Err(());
        }
    }

    loop {
        if let Some(stack_head) = operator_stack.pop() {
            output.push(Token::Operator(stack_head));
        } else {
            break;
        }
    }

    Ok(output)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add() {
        assert_eq!(evaluate_expression("5 + 5"), Ok(10));
    }

    #[test]
    fn subtract() {
        assert_eq!(evaluate_expression("5 - 5"), Ok(0));
    }

    #[test]
    fn divide() {
        assert_eq!(evaluate_expression("5 / 5"), Ok(1));
    }

    #[test]
    fn multiply() {
        assert_eq!(evaluate_expression("5 * 5"), Ok(25));
    }

    #[test]
    fn postfix_addition() {
        let expected = vec![
            Token::Number(5), Token::Number(5), Token::Operator(Operator::Addition)
        ];
        assert_eq!(to_postfix("5 + 5"), Ok(expected));
    }

    #[test]
    fn postfix_add_sub() {
        let expected = vec![
            Token::Number(5), Token::Number(5), Token::Operator(Operator::Addition),
            Token::Number(5), Token::Operator(Operator::Subtraction)
        ];
        assert_eq!(to_postfix("5 + 5 - 5"), Ok(expected));
    }

    #[test]
    fn postfix_add_sub_mul() {
        let expected = vec![
            Token::Number(5), Token::Number(5), Token::Operator(Operator::Addition),
            Token::Number(5), Token::Number(3), Token::Operator(Operator::Multiplication),
            Token::Operator(Operator::Subtraction)
        ];
        assert_eq!(to_postfix("5 + 5 - 5 * 3"), Ok(expected));
    }

    #[test]
    fn postfix_parenthesis() {
        let expected = vec![
            Token::Number(5), Token::Number(5), Token::Number(5),
            Token::Operator(Operator::Subtraction), Token::Number(3),
            Token::Operator(Operator::Multiplication), Token::Operator(Operator::Addition)
        ];
        assert_eq!(to_postfix("5 + ( 5 - 5 ) * 3"), Ok(expected));
    }
}