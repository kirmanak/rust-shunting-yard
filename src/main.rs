extern crate calculator;

use calculator::evaluate_expression;
use point::Point;
use std::io::BufRead;
use std::io::stdin;

mod point;

fn main() {
    let stdin = stdin();
    stdin.lock().lines()
        // make sure the line read properly
        .filter(|res| res.is_ok())
        // unwrap the result
        .map(|res| res.unwrap())
        // ensure the line can possibly contain an expression
        .filter(|line| line.len() > 2)
        // evaluate the expression
        .for_each(|line| {
            println!("Expression is \"{}\"", &line);
            if let Ok(result) = evaluate_expression::<Point<f64>>(&line) {
                println!("Result is \"{:?}\"", result)
            } else {
                eprintln!("Expression is incorrect");
            }
        });
}

