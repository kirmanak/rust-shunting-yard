use std::ops::{Add, Div, Mul, Rem, Sub};
use std::str::FromStr;

#[derive(Clone, Debug)]
pub struct Point<T>
    where T: Add<Output=T> + Sub<Output=T> + Mul<Output=T> + Div<Output=T> + Rem<Output=T>
    + Clone + FromStr {
    x: T,
    y: T,
    z: T,
}

impl<T> Add for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Output = Point<T>;

    fn add(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl<T> Sub for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Output = Point<T>;

    fn sub(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl<T> Mul for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Output = Point<T>;

    fn mul(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
            z: self.z * rhs.z,
        }
    }
}

impl<T> Div for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Output = Point<T>;

    fn div(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x / rhs.x,
            y: self.y / rhs.y,
            z: self.z / rhs.z,
        }
    }
}

impl<T> Rem for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Output = Point<T>;

    fn rem(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x % rhs.x,
            y: self.y % rhs.y,
            z: self.z % rhs.z,
        }
    }
}

impl<T> FromStr for Point<T> where T: Add<Output=T> + Sub<Output=T> +
Mul<Output=T> + Div<Output=T> + Rem<Output=T> + Clone + FromStr {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut iterator = input
            .trim_matches(|c| c == '(' || c == ')')
            .split(',');
        if let Some(x) = iterator.next() {
            if let Some(y) = iterator.next() {
                if let Some(z) = iterator.next() {
                    if let Ok(x) = x.parse() {
                        if let Ok(y) = y.parse() {
                            if let Ok(z) = z.parse() {
                                Ok(Point { x, y, z })
                            } else {
                                Err(())
                            }
                        } else {
                            Err(())
                        }
                    } else {
                        Err(())
                    }
                } else {
                    Err(())
                }
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }
}
