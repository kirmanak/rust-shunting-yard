use operator::Operator;
use std::ops::{Add, Div, Mul, Rem, Sub};
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq)]
pub enum Token<T>
    where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> + FromStr {
    Number(T),
    Operator(Operator),
}

impl<T> FromStr for Token<T>
    where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> + FromStr {
    type Err = ();

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        if let Ok(num) = value.parse::<T>() {
            Ok(Token::Number(num))
        } else if let Ok(operator) = value.parse::<Operator>() {
            Ok(Token::Operator(operator))
        } else {
            Err(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_f64() {
        assert_eq!(Token::from_str("5.43"), Ok(Token::Number(5.43f64)));
    }

    #[test]
    fn parse_mul() {
        assert_eq!(Token::<f64>::from_str("*"), Ok(Token::Operator(Operator::Multiplication)));
    }
}