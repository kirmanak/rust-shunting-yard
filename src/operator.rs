use std::cmp::Ordering;
use std::cmp::PartialEq;
use std::ops::{Add, Div, Mul, Rem, Sub};
use std::str::FromStr;

#[derive(PartialEq, Eq, Debug)]
pub enum Operator {
    Addition,
    Subtraction,
    Multiplication,
    Remainder,
    Division,
    LeftBracket,
    RightBracket,
}

impl Operator {
    pub fn apply<T>(&self, first: T, second: T) -> T
        where T: Add<Output=T> + Sub<Output=T> + Div<Output=T> + Rem<Output=T> + Mul<Output=T> {
        match self {
            Operator::Addition => first + second,
            Operator::Subtraction => first - second,
            Operator::Multiplication => first * second,
            Operator::Remainder => first % second,
            Operator::Division => first / second,
            _ => panic!("The operator {:?} is not applicable", &self),
        }
    }
}

impl PartialOrd for Operator {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Operator {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.eq(other) {
            return Ordering::Equal;
        }
        match self {
            Operator::Subtraction | Operator::Addition => {
                match other {
                    Operator::Subtraction | Operator::Addition => Ordering::Equal,
                    _ => Ordering::Less
                }
            }
            _ => {
                match other {
                    Operator::Subtraction | Operator::Addition => Ordering::Greater,
                    _ => Ordering::Equal
                }
            }
        }
    }
}

impl FromStr for Operator {
    type Err = ();

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "+" => Ok(Operator::Addition),
            "-" => Ok(Operator::Subtraction),
            "*" => Ok(Operator::Multiplication),
            "%" => Ok(Operator::Remainder),
            "/" => Ok(Operator::Division),
            "(" => Ok(Operator::LeftBracket),
            ")" => Ok(Operator::RightBracket),
            _ => Err(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cmp_mul_add() {
        assert_eq!(Operator::Multiplication.cmp(&Operator::Addition), Ordering::Greater);
    }

    #[test]
    fn cmp_mul_sub() {
        assert_eq!(Operator::Multiplication.cmp(&Operator::Subtraction), Ordering::Greater);
    }

    #[test]
    fn cmp_mul_div() {
        assert_eq!(Operator::Multiplication.cmp(&Operator::Division), Ordering::Equal);
    }

    #[test]
    fn cmp_mul_mul() {
        assert_eq!(Operator::Multiplication.cmp(&Operator::Multiplication), Ordering::Equal);
    }
}
